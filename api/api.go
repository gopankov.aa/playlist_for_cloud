package api

//import (
//	"encoding/json"
//	"net/http"
//
//	"playlist/internal/playlist"
//	"time"
//
//	"github.com/gorilla/mux"
//)
//
//type songData struct {
//	Name     string        `json:"name"`
//	Duration time.Duration `json:"duration"`
//}
//
//func getSongsHandler(w http.ResponseWriter, r *http.Request) {
//	pl := playlist.NewPlaylist()
//	songs := make([]*songData, 0)
//	for node := pl.Current(); node != nil; node = node.Next {
//		songs = append(songs, &songData{Name: node.Song.Name, Duration: node.Song.Duration})
//	}
//	json.NewEncoder(w).Encode(songs)
//}
//
//func getSongHandler(w http.ResponseWriter, r *http.Request) {
//	pl := playlist.NewPlaylist()
//	params := mux.Vars(r)
//	name := params["name"]
//	for node := pl.Current(); node != nil; node = node.Next {
//		if node.Song.Name == name {
//			json.NewEncoder(w).Encode(&songData{Name: node.Song.Name, Duration: node.Song.Duration})
//			return
//		}
//	}
//	http.NotFound(w, r)
//}
//
//func addSongHandler(w http.ResponseWriter, r *http.Request) {
//	pl := playlist.NewPlaylist()
//	var data songData
//	err := json.NewDecoder(r.Body).Decode(&data)
//	if err != nil {
//		http.Error(w, err.Error(), http.StatusBadRequest)
//		return
//	}
//	pl.AddSong(&playlist.Song{Name: data.Name, Duration: data.Duration})
//	w.WriteHeader(http.StatusCreated)
//}
//
//func updateSongHandler(w http.ResponseWriter, r *http.Request) {
//	pl := playlist.NewPlaylist()
//	var data songData
//	err := json.NewDecoder(r.Body).Decode(&data)
//	if err != nil {
//		http.Error(w, err.Error(), http.StatusBadRequest)
//		return
//	}
//	params := mux.Vars(r)
//	name := params["name"]
//	for node := pl.Current(); node != nil; node = node.Next {
//		if node.Song.Name == name {
//			node.Song.Name = data.Name
//			node.Song.Duration = data.Duration
//			w.WriteHeader(http.StatusOK)
//			return
//		}
//	}
//	http.NotFound(w, r)
//}
