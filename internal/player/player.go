package player

import (
	"bufio"
	"fmt"
	"os"
	"playlist/internal/playlist"
)

type Player struct {
	playlist   *playlist.Playlist
	paused     bool
	pause      bool
	command    chan string
	stopSignal chan struct{}
}

func NewPlayer(pl *playlist.Playlist) *Player {
	return &Player{
		playlist:   pl,
		paused:     false,
		pause:      false,
		command:    make(chan string),
		stopSignal: make(chan struct{}),
	}
}

func (p *Player) Play() {
	go p.playlist.Play()
}

func (p *Player) SetPause(pause bool) {
	p.pause = pause
	if !p.pause {
		p.playlist.Resume <- struct{}{}
	}
}

func (p *Player) Pause() {
	p.playlist.Pause()
	p.paused = true
}

func (p *Player) Run(done chan bool) {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print("Type 'Play' to start playing songs: ")

	scannerChan := make(chan string)

	go func() {
		for scanner.Scan() {
			scannerChan <- scanner.Text()
		}
	}()

	go p.handleCommand()

	for {
		select {
		case input := <-scannerChan:
			switch input {
			case "Play":
				p.command <- "play"
			case "Pause":
				p.command <- "pause"
			case "Resume":
				p.command <- "resume"
			}
			fmt.Print("Type 'Play' to start playing songs: ")
		case <-p.command:
			if p.pause {
				return
			}
		case <-done:
			p.playlist.Pause()
			return
		}
	}
}

func (p *Player) handleCommand() {
	for {
		command := <-p.command
		switch command {
		case "play":
			go p.playlist.Play()
		case "pause":
			if p.playlist.IsPlaying() {
				p.command <- "pause"
				p.Pause()
			}
		case "resume":
			p.SetPause(false)
		}
	}
}

func (p *Player) IsPlaying() bool {
	return p.playlist.IsPlaying()
}

func (p *Player) GetCommand() chan string {
	return p.command
}
