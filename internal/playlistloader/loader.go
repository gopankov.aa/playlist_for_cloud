package playlistloader

import (
	"encoding/xml"
	"io/ioutil"
	"net/http"
	"playlist/internal/playlist"
	"strings"
	"time"
)

type Response struct {
	XMLName  xml.Name  `xml:"ListBucketResult"`
	Contents []Content `xml:"Contents"`
}

type Content struct {
	Key string `xml:"Key"`
}

func LoadPlaylistFromXML(url string) (*playlist.Playlist, error) {
	pl := playlist.NewPlaylist()

	resp, err := http.Get(url)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}

	var r Response
	err = xml.Unmarshal(body, &r)
	if err != nil {
		return nil, err
	}

	for _, c := range r.Contents {
		if strings.HasSuffix(c.Key, ".mp3") {
			pl.AddSong(&playlist.Song{Name: c.Key, Duration: 5 * time.Second, Path: url + "/" + c.Key})
		}
	}

	return pl, nil
}
