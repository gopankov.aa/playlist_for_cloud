package playlist

import (
	_ "encoding/binary"
	"fmt"
	"github.com/hajimehoshi/go-mp3"
	"github.com/hajimehoshi/oto"
	"io"
	"net/http"
	"sync"
	"time"
)

type Song struct {
	Name     string
	Duration time.Duration
	Path     string
}

type Node struct {
	Song *Song
	Prev *Node
	Next *Node
}

type Playlist struct {
	mu       sync.RWMutex
	head     *Node
	tail     *Node
	current  *Node
	playing  bool
	paused   bool
	pause    bool
	finished chan struct{}
	resume   chan interface{}
	Resume   chan struct{}
}

func NewPlaylist() *Playlist {
	return &Playlist{
		finished: make(chan struct{}),
		resume:   make(chan interface{}),
		Resume:   make(chan struct{}),
	}
}

func (pl *Playlist) Play() {
	pl.mu.Lock()
	defer pl.mu.Unlock()

	if pl.playing {
		return
	}

	pl.playing = true
	pl.paused = false

	go func() {
		for pl.playing {
			if !pl.paused && !pl.pause {
				pl.playCurrent()
			} else if pl.paused {
				select {
				case <-pl.Resume:
					pl.paused = false
				default:
				}
			}
		}
	}()
}

func (pl *Playlist) Pause() {
	pl.mu.Lock()
	defer pl.mu.Unlock()

	if !pl.playing {
		return
	}

	pl.paused = true
}

func (pl *Playlist) AddSong(song *Song) {
	pl.mu.Lock()
	defer pl.mu.Unlock()

	node := &Node{Song: song}

	if pl.head == nil {
		pl.head = node
		pl.tail = node
	} else {
		pl.tail.Next = node
		node.Prev = pl.tail
		pl.tail = node
	}

	if pl.current == nil {
		pl.current = pl.head
	}
}

func (pl *Playlist) Next() {
	pl.mu.Lock()
	defer pl.mu.Unlock()

	if pl.current == nil {
		pl.current = pl.head
	} else {
		pl.current = pl.current.Next
	}

	if pl.current == nil {
		return
	}

	pl.paused = true
	pl.playCurrent()
}

func (pl *Playlist) playCurrent() error {
	if pl.current == nil || pl.paused {
		return nil
	}

	resp, err := http.Get(pl.current.Song.Path)
	if err != nil {
		return fmt.Errorf("error fetching audio file: %v", err)
	}
	defer resp.Body.Close()

	decoder, err := mp3.NewDecoder(resp.Body)
	if err != nil {
		return fmt.Errorf("error creating mp3 decoder: %v", err)
	}

	context, err := oto.NewContext(decoder.SampleRate(), 2, 2, 8192)
	if err != nil {
		return fmt.Errorf("error creating audio context: %v", err)
	}

	player := context.NewPlayer()
	defer player.Close()

	if _, err := io.Copy(player, decoder); err != nil {
		return fmt.Errorf("error playing audio: %v", err)
	}

	pl.finished <- struct{}{}

	return nil
}

func (pl *Playlist) IsPlaying() bool {
	return pl.playing
}

func (pl *Playlist) Lock() {
	pl.mu.Lock()
}

func (pl *Playlist) Unlock() {
	pl.mu.Unlock()
}
