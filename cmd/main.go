package main

import (
	"bufio"
	"fmt"
	"log"
	"os"
	"playlist/internal/player"
	"playlist/internal/playlistloader"
)

func main() {
	pl, err := playlistloader.LoadPlaylistFromXML("https://music.obs.ru-moscow-1.hc.sbercloud.ru")
	if err != nil {
		log.Fatal(err)
	}

	p := player.NewPlayer(pl)

	// Start listening for user input
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Print("Type 'Play' to start playing songs: ")
	for scanner.Scan() {
		switch scanner.Text() {
		case "Play":
			// Start playing songs
			go p.Play()
			go p.Run(make(chan bool))
		case "Pause":
			pauseText := "Playback stopped. Type 'Resume' to continue playing songs: "
			if p.IsPlaying() {
				p.GetCommand() <- "pause"
				p.Pause()
			} else {
				pauseText = "Playback not started. Type 'Play' to start playing songs: "
			}
			fmt.Print(pauseText)
		case "Resume":
			p.SetPause(false)
			fmt.Print("Type 'Play' to start playing songs: ")
		case "Next":
			p.GetCommand() <- "next"
		case "Prev":
			p.GetCommand() <- "prev"
		}
		fmt.Print("Type 'Play' to start playing songs: ")
	}
}
